import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { FamilyService } from '../../services/family.service';

@Component({
  selector: 'lab-js-add-family',
  templateUrl: './add-family.component.html',
  styleUrls: ['./add-family.component.scss']
})
export class AddFamilyComponent implements OnInit, OnDestroy {
  public familyForm: FormGroup;
  private formSubscription: Subscription;

  public get children() {
    return this.familyForm.get('children') as FormArray;
  }

  public constructor(
    private readonly familyService: FamilyService
  ) { }

  public ngOnInit(): void {
    this.initForm();
  }

  public ngOnDestroy(): void {
    if (this.formSubscription) {
      this.formSubscription.unsubscribe()
    }
  }

  public generateFamilyMember() {
    return {
      name: new FormControl(null, [Validators.required]),
      age: new FormControl(null, [Validators.required])
    }
  }

  public initForm() {
    return this.familyForm = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      father: new FormGroup(this.generateFamilyMember()),
      mother: new FormGroup(this.generateFamilyMember()),
      children: new FormArray([new FormGroup(this.generateFamilyMember())])
    })
  }

  public addChild() {
    const child = new FormGroup(this.generateFamilyMember());
    this.children.push(child);
  }
  public removeChild(index: number) {
    this.children.removeAt(index);
  }
  public submit() {
    this.formSubscription = this.familyService.addFamily$(this.familyForm.value).subscribe(
      () => {
        this.familyForm.reset();
      },
      (err) => {
        console.log('error', err)
      })
  }
}
